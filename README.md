# Restaurant UI Gateway

Simple UI for [Restaurant service](https://gitlab.com/foodprojectdemo/restaurant-service). Here restaurateurs can accept
tickets for cooking.

### Development mode

Run with profile ``--spring.profiles.active=dev`` to active restaurant-stub.

Use the following to login: successful_login@mail.com/password