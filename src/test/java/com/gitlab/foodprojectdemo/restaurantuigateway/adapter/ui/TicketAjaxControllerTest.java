package com.gitlab.foodprojectdemo.restaurantuigateway.adapter.ui;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import com.gitlab.foodprojectdemo.restaurantuigateway.AbstractTest;
import com.gitlab.foodprojectdemo.restaurantuigateway.domain.RestaurantService;

import java.util.Map;
import java.util.UUID;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = TicketAjaxController.class)
@WithMockUser
class TicketAjaxControllerTest extends AbstractTest {

  @Autowired private MockMvc mockMvc;

  @Autowired private ObjectMapper objectMapper;

  @MockBean private RestaurantService restaurantService;

  @Test
  void shouldChangeStatus() throws Exception {
    mockMvc
        .perform(
            post("/tickets/{orderId}/status", UUID.randomUUID())
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(Map.of("status", "accepted"))))
        .andDo(print())
        .andExpect(status().isOk());
  }
}
