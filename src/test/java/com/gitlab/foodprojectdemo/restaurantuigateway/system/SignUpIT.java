package com.gitlab.foodprojectdemo.restaurantuigateway.system;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import org.apache.http.HttpHeaders;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import com.gitlab.foodprojectdemo.restaurantuigateway.AbstractTest;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static com.gitlab.foodprojectdemo.restaurantuigateway.Faker.faker;
import static com.gitlab.foodprojectdemo.restaurantuigateway.fixture.RestaurateurFixture.restaurateur;

@SpringBootTest
@DirtiesContext
@AutoConfigureMockMvc
@AutoConfigureWireMock(port = 0)
@TestPropertySource(properties = "restaurant-service.url=http://localhost:${wiremock.server.port}/")
class SignUpIT extends AbstractTest {
  @Autowired private MockMvc mockMvc;

  @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
  @Autowired
  private WireMockServer mockServer;

  @Autowired private ObjectMapper objectMapper;

  @Test
  void auth() throws Exception {
    var restaurateur = restaurateur();
    mockServer.stubFor(
        post("/api/v1/restaurateurs/auth")
            .willReturn(
                aResponse()
                    .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    .withBody(objectMapper.writeValueAsString(restaurateur))));

    mockMvc
        .perform(formLogin().user(restaurateur.getLogin()).password(faker().internet().password()))
        .andExpect(authenticated().withUsername(restaurateur.getLogin()));
  }
}
