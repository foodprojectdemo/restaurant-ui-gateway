package com.gitlab.foodprojectdemo.restaurantuigateway.contracts.restaurant;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import com.gitlab.foodprojectdemo.restaurantuigateway.AbstractTest;
import com.gitlab.foodprojectdemo.restaurantuigateway.domain.NewStatus;
import com.gitlab.foodprojectdemo.restaurantuigateway.domain.RestaurantService;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static com.gitlab.foodprojectdemo.restaurantuigateway.fixture.IdFixture.id;

@TestConfiguration
@ComponentScan("com.gitlab.foodprojectdemo.restaurantuigateway.adapter.http.gateway.restaurant")
class RestaurantGatewayConfig {}

@Slf4j
@SpringBootTest
@DirtiesContext
@AutoConfigureWireMock(
    stubs =
        "classpath:/META-INF/com.gitlab.foodprojectdemo/restaurant-service/**/mappings/**/*.json",
    port = 0)
@ContextConfiguration(classes = RestaurantGatewayConfig.class)
@TestPropertySource(properties = "restaurant-service.url=http://localhost:${wiremock.server.port}/")
class RestaurantServiceIT extends AbstractTest {

  private static final String SUCCESSFUL_LOGIN = "successful_login@mail.com";
  private static final String SUCCESSFUL_PASSWORD = "password";
  private static final String FAILED_LOGIN = "failed_login@mail.com";
  private static final String FAILED_PASSWORD = "failed_password";

  @Autowired private RestaurantService restaurantService;

  @Test
  void shouldAuth() {
    var restaurateur = restaurantService.auth(SUCCESSFUL_LOGIN, SUCCESSFUL_PASSWORD);
    assertThat(restaurateur.isPresent()).isTrue();
  }

  @Test
  void shouldNotAuth() {
    var restaurateur = restaurantService.auth(FAILED_LOGIN, FAILED_PASSWORD);
    assertThat(restaurateur.isEmpty()).isTrue();
  }

  @Test
  void shouldReturnDishes() {
    var dishes = restaurantService.dishes(id());
    assertThat(dishes).isNotEmpty();
  }

  @Test
  void shouldReturnRestaurant() {
    var restaurant = restaurantService.restaurant(id());
    assertThat(restaurant).isNotNull();
  }

  @Test
  void shouldReturnCategories() {
    var categories = restaurantService.categories(id());
    assertThat(categories).isNotEmpty();
  }

  @Test
  void shouldReturnTickets() {
    var tickets = restaurantService.tickets(id());
    assertThat(tickets).isNotEmpty();
  }

  @Test
  void shouldChangeTicketStatus() {
    restaurantService.changeStatus(id(), UUID.randomUUID().toString(), NewStatus.ACCEPTED);
  }
}
