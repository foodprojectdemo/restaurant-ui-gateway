package com.gitlab.foodprojectdemo.restaurantuigateway.fixture;

import com.gitlab.foodprojectdemo.restaurantuigateway.domain.Restaurateur;

import static com.gitlab.foodprojectdemo.restaurantuigateway.Faker.faker;

public class RestaurateurFixture {

  public static Restaurateur restaurateur() {
    return new Restaurateur(
        IdFixture.id(), IdFixture.id(), faker().name().name(), faker().internet().emailAddress());
  }
}
