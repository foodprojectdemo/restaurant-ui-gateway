package com.gitlab.foodprojectdemo.restaurantuigateway.fixture;

import static com.gitlab.foodprojectdemo.restaurantuigateway.Faker.faker;

public class IdFixture {

  public static Long id() {
    return faker().number().numberBetween(1L, 1000L);
  }
}
