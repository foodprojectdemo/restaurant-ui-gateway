package com.gitlab.foodprojectdemo.restaurantuigateway.domain;

import lombok.NonNull;
import lombok.Value;

@Value
public class Category {
  @NonNull Long id;
  @NonNull Long restaurantId;
  @NonNull String name;
}
