package com.gitlab.foodprojectdemo.restaurantuigateway.domain;

public enum NewStatus {
  ACCEPTED,
  REJECTED,
  READY
}
