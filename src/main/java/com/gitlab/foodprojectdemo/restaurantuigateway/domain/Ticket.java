package com.gitlab.foodprojectdemo.restaurantuigateway.domain;

import lombok.NonNull;
import lombok.Value;

import java.util.Collection;

@Value
public class Ticket {
  @Value
  public static class Item {
    @NonNull Long dishId;
    @NonNull String name;
    @NonNull Integer quantity;
  }

  @NonNull String id;
  @NonNull String status;
  @NonNull Collection<Item> items;
}
