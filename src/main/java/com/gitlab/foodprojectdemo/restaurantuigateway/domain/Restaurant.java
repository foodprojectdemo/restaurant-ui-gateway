package com.gitlab.foodprojectdemo.restaurantuigateway.domain;

import lombok.NonNull;
import lombok.Value;

import java.util.HashSet;
import java.util.Set;

@Value
public class Restaurant {

  @Value
  public static class LegalData {
    @NonNull String address;
    @NonNull String inn;
    @NonNull String name;
    @NonNull String ogrn;
  }

  @NonNull Long id;
  @NonNull String name;
  @NonNull String information;
  @NonNull String logo;
  @NonNull String openDays;
  @NonNull String openFrom;
  @NonNull String openTo;
  @NonNull LegalData legalData;
  @NonNull Set<String> cuisines = new HashSet<>();
}
