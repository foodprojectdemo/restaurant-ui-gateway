package com.gitlab.foodprojectdemo.restaurantuigateway.domain;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface RestaurantService {

  Optional<Restaurateur> auth(String login, String password);

  Restaurant restaurant(Long restaurantId);

  Collection<Dish> dishes(Long restaurantId);

  Collection<Category> categories(Long restaurantId);

  List<Ticket> tickets(Long restaurantId);

  void changeStatus(Long restaurantId, String orderId, NewStatus status);
}
