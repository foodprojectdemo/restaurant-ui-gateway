package com.gitlab.foodprojectdemo.restaurantuigateway.domain;

import lombok.NonNull;
import lombok.Value;

import java.io.Serializable;

@Value
public class Restaurateur {
  @NonNull Long id;
  @NonNull Long restaurantId;
  @NonNull String name;
  @NonNull String login;
}
