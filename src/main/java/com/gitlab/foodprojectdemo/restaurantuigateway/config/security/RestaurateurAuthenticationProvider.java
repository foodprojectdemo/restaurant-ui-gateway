package com.gitlab.foodprojectdemo.restaurantuigateway.config.security;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import com.gitlab.foodprojectdemo.restaurantuigateway.domain.RestaurantService;
import com.gitlab.foodprojectdemo.restaurantuigateway.domain.Restaurateur;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Slf4j
@Component
@AllArgsConstructor
class RestaurateurAuthenticationProvider implements AuthenticationProvider {
  private final RestaurantService restaurantService;

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    var name = authentication.getName();
    var password = authentication.getCredentials().toString();
    if (name.isEmpty() || password.isEmpty()) {
      throw new BadCredentialsException("Login or password cannot be empty");
    }

    Optional<Restaurateur> restaurateurOptional;
    try {
      restaurateurOptional = restaurantService.auth(name, password);
    } catch (RuntimeException exception) {
      throw new AuthenticationServiceException(exception.getMessage());
    }
    if (restaurateurOptional.isEmpty()) {
      throw new BadCredentialsException("Invalid login or password");
    }
    var restaurateur = restaurateurOptional.get();

    var authorities = List.of(new SimpleGrantedAuthority(ROLE.ROLE_USER.name()));
    var token = new UsernamePasswordAuthenticationToken(name, password, authorities);
    token.setDetails(new RestaurateurToken(restaurateur));

    return token;
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return authentication.equals(UsernamePasswordAuthenticationToken.class);
  }
}
