package com.gitlab.foodprojectdemo.restaurantuigateway.config.jackson;

class IllegalValueObjectException extends IllegalArgumentException {

  IllegalValueObjectException(String message, Throwable cause) {
    super(message, cause);
  }
}
