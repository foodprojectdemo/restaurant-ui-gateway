package com.gitlab.foodprojectdemo.restaurantuigateway.config.security;

import lombok.NonNull;
import lombok.Value;
import com.gitlab.foodprojectdemo.restaurantuigateway.domain.Restaurateur;

import java.io.Serializable;

@Value
public class RestaurateurToken implements Serializable {
  @NonNull Long id;
  @NonNull Long restaurantId;
  @NonNull String name;
  @NonNull String login;

  public RestaurateurToken(@NonNull Restaurateur restaurateur) {
    this.id = restaurateur.getId();
    this.restaurantId = restaurateur.getRestaurantId();
    this.name = restaurateur.getName();
    this.login = restaurateur.getLogin();
  }
}
