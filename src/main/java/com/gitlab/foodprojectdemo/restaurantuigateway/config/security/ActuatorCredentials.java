package com.gitlab.foodprojectdemo.restaurantuigateway.config.security;

import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

@Value
@ConstructorBinding
@ConfigurationProperties("actuator")
class ActuatorCredentials {
  String login;
  CharSequence password;

  public String getLogin() {
    return login != null ? login : "actuator";
  }

  public CharSequence getPassword() {
    return password != null ? password : "actuator";
  }
}
