package com.gitlab.foodprojectdemo.restaurantuigateway.config.security;

enum ROLE {
  ROLE_MONITORING,
  ROLE_USER
}
