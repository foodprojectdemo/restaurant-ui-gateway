package com.gitlab.foodprojectdemo.restaurantuigateway.config.security;

import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Slf4j
@EnableWebSecurity
public class SecurityConfig {

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Order(1)
  @Configuration
  @AllArgsConstructor
  @EnableConfigurationProperties(ActuatorCredentials.class)
  class ActuatorHttpBasicConfig extends WebSecurityConfigurerAdapter {
    private final PasswordEncoder passwordEncoder;
    private final ActuatorCredentials credentials;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
      http.requestMatcher(EndpointRequest.toAnyEndpoint())
          .authorizeRequests()
          .anyRequest()
          .hasAuthority(ROLE.ROLE_MONITORING.name())
          .and()
          .httpBasic();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
      auth.inMemoryAuthentication()
          .withUser(credentials.getLogin())
          .password(passwordEncoder.encode(credentials.getPassword()))
          .authorities(ROLE.ROLE_MONITORING.name());
    }
  }

  @Order(2)
  @Configuration
  @AllArgsConstructor
  class AppFormLoginConfig extends WebSecurityConfigurerAdapter {

    private final RestaurateurAuthenticationProvider authenticationProvider;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
      http.authorizeRequests()
          .antMatchers("/error/**", "/webjars/**", "/css/**", "/js/**")
          .permitAll()
          .anyRequest()
          .hasAuthority(ROLE.ROLE_USER.name())
          .and()
          .formLogin();
    }

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) {
      auth.authenticationProvider(authenticationProvider);
    }
  }
}
