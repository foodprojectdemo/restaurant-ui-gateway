package com.gitlab.foodprojectdemo.restaurantuigateway.adapter.http.gateway.restaurant;

import lombok.NonNull;
import lombok.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

@Value
@ConstructorBinding
@ConfigurationProperties(prefix = "restaurant-service")
class RestaurantGatewayProperties {
  @NonNull String url;
  @NonNull String apiBaseUri;
  @NonNull String restaurateurSearchUri;
  @NonNull String restaurateurAuthUri;
  @NonNull String restaurantUri;
  @NonNull String restaurantDishesUri;
  @NonNull String restaurantCategoriesUri;
  @NonNull String restaurantTicketsUri;
  @NonNull String statusTicketUri;

  public String getBaseUri() {
    return url + apiBaseUri;
  }
}
