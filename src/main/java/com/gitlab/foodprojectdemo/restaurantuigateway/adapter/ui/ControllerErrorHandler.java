package com.gitlab.foodprojectdemo.restaurantuigateway.adapter.ui;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
@ControllerAdvice
public class ControllerErrorHandler {

  @ExceptionHandler(Exception.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  private String exception(Exception e, Model model) {
    log.error(e.getMessage(), e);
    model.addAttribute("error", "Internal error");
    return "error/500";
  }
}
