package com.gitlab.foodprojectdemo.restaurantuigateway.adapter.http.gateway.restaurant;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import com.gitlab.foodprojectdemo.restaurantuigateway.domain.Category;
import com.gitlab.foodprojectdemo.restaurantuigateway.domain.Dish;
import com.gitlab.foodprojectdemo.restaurantuigateway.domain.NewStatus;
import com.gitlab.foodprojectdemo.restaurantuigateway.domain.Restaurant;
import com.gitlab.foodprojectdemo.restaurantuigateway.domain.RestaurantService;
import com.gitlab.foodprojectdemo.restaurantuigateway.domain.Restaurateur;
import com.gitlab.foodprojectdemo.restaurantuigateway.domain.Ticket;
import com.gitlab.foodprojectdemo.webclienterrorhandlerspringbootstarter.AuthenticationException;
import reactor.core.publisher.Mono;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Component
@EnableConfigurationProperties(RestaurantGatewayProperties.class)
class RestaurantGateway implements RestaurantService {
  private final WebClient webClient;
  private final RestaurantGatewayProperties properties;

  public RestaurantGateway(WebClient.Builder builder, RestaurantGatewayProperties properties) {
    this.webClient =
        builder
            .baseUrl(properties.getBaseUri())
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .build();
    this.properties = properties;
  }

  @Override
  public Optional<Restaurateur> auth(final String login, final String password) {
    var params = Map.of("login", login, "password", password);

    return Optional.ofNullable(
        webClient
            .post()
            .uri(properties.getRestaurateurAuthUri())
            .bodyValue(params)
            .retrieve()
            .bodyToMono(Restaurateur.class)
            .onErrorResume(AuthenticationException.class, exception -> Mono.empty())
            .block());
  }

  @Override
  public Restaurant restaurant(Long restaurantId) {
    return webClient
        .get()
        .uri(properties.getRestaurantUri(), restaurantId)
        .retrieve()
        .bodyToMono(Restaurant.class)
        .block();
  }

  @Override
  public Collection<Dish> dishes(Long restaurantId) {
    return webClient
        .get()
        .uri(properties.getRestaurantDishesUri(), restaurantId)
        .retrieve()
        .bodyToFlux(Dish.class)
        .collectList()
        .block();
  }

  @Override
  public Collection<Category> categories(Long restaurantId) {
    return webClient
        .get()
        .uri(properties.getRestaurantCategoriesUri(), restaurantId)
        .retrieve()
        .bodyToFlux(Category.class)
        .collectList()
        .block();
  }

  @Override
  public List<Ticket> tickets(Long restaurantId) {
    return webClient
        .get()
        .uri(properties.getRestaurantTicketsUri(), restaurantId)
        .retrieve()
        .bodyToFlux(Ticket.class)
        .collectList()
        .block();
  }

  @Override
  public void changeStatus(Long restaurantId, String orderId, NewStatus status) {
    webClient
        .patch()
        .uri(properties.getStatusTicketUri(), restaurantId, orderId)
        .bodyValue(Map.of("status", status))
        .retrieve()
        .toBodilessEntity()
        .block();
  }
}
