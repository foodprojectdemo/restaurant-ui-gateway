package com.gitlab.foodprojectdemo.restaurantuigateway.adapter.ui;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import com.gitlab.foodprojectdemo.restaurantuigateway.domain.RestaurantService;

@Slf4j
@Controller
@AllArgsConstructor
public class CategoriesController {
  private final RestaurantService restaurantService;

  @GetMapping("/categories")
  public String categories(Model model) {
    var restaurateur = AuthenticatedRestaurateurHolder.getAuthenticatedRestaurateur();

    var categories = restaurantService.categories(restaurateur.getRestaurantId());

    model.addAttribute("categories", categories);
    return "categories";
  }
}
