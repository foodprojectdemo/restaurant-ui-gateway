package com.gitlab.foodprojectdemo.restaurantuigateway.adapter.ui;

import lombok.Value;

@Value
public class AjaxAnswer {
  private static final String SUCCESS = "SUCCESS";
  private static final String FAILURE = "FAILURE";
  String status;
  Object data;

  @Value
  public static class Error {
    String message;
  }

  private AjaxAnswer(String status, Object data) {
    this.status = status;
    this.data = data;
  }

  public static AjaxAnswer success() {
    return success(null);
  }

  public static AjaxAnswer success(Object data) {
    return new AjaxAnswer(SUCCESS, data);
  }

  public static AjaxAnswer failure(Throwable throwable) {
    return new AjaxAnswer(FAILURE, new Error(throwable.getMessage()));
  }
}
