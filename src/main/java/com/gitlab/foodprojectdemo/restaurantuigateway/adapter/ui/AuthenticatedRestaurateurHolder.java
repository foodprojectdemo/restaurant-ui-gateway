package com.gitlab.foodprojectdemo.restaurantuigateway.adapter.ui;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import com.gitlab.foodprojectdemo.restaurantuigateway.config.security.RestaurateurToken;

class AuthenticatedRestaurateurHolder {

  static RestaurateurToken getAuthenticatedRestaurateur() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (!authentication.isAuthenticated()) {
      throw new AccessDeniedException("User not authorized");
    }

    if (!(authentication.getDetails() instanceof RestaurateurToken)) {
      throw new RuntimeException(
          "Invalid Token Details: " + authentication.getDetails().getClass());
    }

    return (RestaurateurToken) authentication.getDetails();
  }
}
