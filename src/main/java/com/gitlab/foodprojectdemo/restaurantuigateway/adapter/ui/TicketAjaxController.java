package com.gitlab.foodprojectdemo.restaurantuigateway.adapter.ui;

import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.gitlab.foodprojectdemo.restaurantuigateway.domain.NewStatus;
import com.gitlab.foodprojectdemo.restaurantuigateway.domain.RestaurantService;

@Value
class StatusDto {
  NewStatus status;
}

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/tickets/{orderId}/status")
public class TicketAjaxController {
  private final RestaurantService restaurantService;

  @PostMapping
  public AjaxAnswer changeStatus(@PathVariable String orderId, @RequestBody StatusDto statusDto) {
    var restaurateur = AuthenticatedRestaurateurHolder.getAuthenticatedRestaurateur();

    restaurantService.changeStatus(restaurateur.getRestaurantId(), orderId, statusDto.getStatus());

    return AjaxAnswer.success();
  }

  @ExceptionHandler
  AjaxAnswer exceptionHandler(Throwable throwable) {
    return AjaxAnswer.failure(throwable);
  }
}
