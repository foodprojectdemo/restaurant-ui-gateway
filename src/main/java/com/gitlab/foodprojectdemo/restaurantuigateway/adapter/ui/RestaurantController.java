package com.gitlab.foodprojectdemo.restaurantuigateway.adapter.ui;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.gitlab.foodprojectdemo.restaurantuigateway.domain.RestaurantService;

@Slf4j
@Controller
@AllArgsConstructor
@RequestMapping("/restaurant")
public class RestaurantController {
  private final RestaurantService restaurantService;

  @GetMapping
  public String restaurant(Model model) {
    var restaurateur = AuthenticatedRestaurateurHolder.getAuthenticatedRestaurateur();

    var restaurant = restaurantService.restaurant(restaurateur.getRestaurantId());

    model.addAttribute("restaurant", restaurant);
    return "restaurant";
  }
}
