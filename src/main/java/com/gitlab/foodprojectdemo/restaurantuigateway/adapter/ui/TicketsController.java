package com.gitlab.foodprojectdemo.restaurantuigateway.adapter.ui;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.gitlab.foodprojectdemo.restaurantuigateway.domain.RestaurantService;

@Slf4j
@Controller
@AllArgsConstructor
@RequestMapping("/tickets")
public class TicketsController {
  private final RestaurantService restaurantService;

  @GetMapping
  public String orders(Model model) {
    var restaurateur = AuthenticatedRestaurateurHolder.getAuthenticatedRestaurateur();
    var tickets = restaurantService.tickets(restaurateur.getRestaurantId());

    model.addAttribute("tickets", tickets);
    return "tickets";
  }
}
