package com.gitlab.foodprojectdemo.restaurantuigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestaurantUiGatewayApplication {

  public static void main(String[] args) {
    SpringApplication.run(RestaurantUiGatewayApplication.class, args);
  }
}
