function errorHandler(jqXHR) {
    const err = JSON.parse(jqXHR.responseText);
    if (err.data.message !== undefined) {
        $('.toast .toast-body').text(err.data.message);
    } else {
        console.log(jqXHR);
        console.log(err);
    }

    $('.toast').toast('show');
}

function failureHandler(data) {
    $('.toast .toast-body').text(data.message);
    $('.toast').toast('show');
}

$(function () {
    $(document).on('submit', '.ticket-status-form', function () {
        $.ajax({
            type: "POST",
            url: $(this).attr("action"),
            data: JSON.stringify({status: $(this).find("input[name='status']").val()}),
            dataType: 'json',
            contentType: "application/json",
            headers: {"X-CSRF-TOKEN": $(this).find("input[name='_csrf']").val()},
            success: function (data) {
                if (data.status === "FAILURE") {
                    failureHandler(data.data)
                }
                window.location.reload();
            },
            error: errorHandler
        });
        return false;
    });
});
